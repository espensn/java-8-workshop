# README #

### Tasks for Java 8 workshop ###

* All tasks is created as JUnit tests
* All tasks should be solved as one liners. (Only one ; in your answer)
* The tasks is self correcting (asserts)
* Do not cheat
* Solutions for tasks on request

### How do I get set up? ###

* Easiest way is to use your favourite IDE (Idea/Eclipse) and run each test individually with the IDE
* Alternatively all tests can be executed with maven (mvn test)