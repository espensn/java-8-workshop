import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class OneLinerJava8WorkshopChallengeTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void numbersWithUnderscoresAreFunny() {
        int i = 1_000_000;

        //Ingen oppgave. Kun en fun fact!

        assertEquals(i, 1000000);
    }

    @Test
    public void printNamesInList() {
        Stream<String> stream = Stream.of("Bettine", "Robert", "Anne");

        //Oppgave: Print navnet på alle personene i listen og print dette til System.out.

        /** ## Din kode her ## **/



        /** ## Din kode her ## **/


        assertEquals("BettineRobertAnne", systemOutRule.getLog());
    }

    @Test
    public void testCountSkills() {
        //Oppgave: Tell antall navn med tre bokstaver som starter på "O".

        Stream<String> stream = Stream.of("Ole", "Bjarne", "Ulf", "Espen", "Oda", "Fia", "Odd", "Pia", "Mons");

        /** ## Din kode her ## **/

        long count = 0; //<--- Bytt ut 0 med din kode

        /** ## Din kode her ## **/

        assertEquals(3, count);
    }

    @Test
    public void testReduceSkills() {
        Stream<Integer> stream = Stream.of(10, 30, 50, 90, 70, 80, 60, 40, 20);

        //Oppgave: Finn summen av de tre midterste tallene i stigende rekkefølge

        /** ## Din kode her ## **/

        long result = 0; //<--- Bytt ut 0 med din kode

        /** ## Din kode her ## **/

        assertEquals(150, result);
    }

    @Test
    public void testFindAnySkills() {
        Stream<Integer> stream = Stream.of(10, 30, 50, 90, 92, 70, 80, 60, 92, 40, 20);

        //Oppgave: Finn et tall som er mellom 90 og 100 i streamen.

        /** ## Din kode her ## **/

        int result = 0; //<--- Bytt ut 0 med din kode

        /** ## Din kode her ## **/

        assertEquals(92, result);
    }

    @Test
    public void testFindFirstSkills() {
        Stream<Integer> stream = Stream.generate(new Random()::nextInt);

        //Oppgave: Finn første tall over 900000000

        /** ## Din kode her ## **/

        int result = 0; //<--- Bytt ut 0 med din kode

        /** ## Din kode her ## **/

        assertEquals(result > 900000000, true);
    }

    @Test
    public void testForEachSkills() {
        Stream<String> stream = Stream.of("Bettine", "Robert", "Anne");

        //Oppgave: Print antall tegn i navnet til de to personene med navn som kommer først alfabetisk

        /** ## Din kode her ## **/



        /** ## Din kode her ## **/

        assertEquals("47", systemOutRule.getLog()); //Anne = 4, Bettine = 7
    }

    @Test
    public void testIntermediateSkills() {
        Stream<String> stream = Stream.of("Bettine", "Robert", "Anne");

        //Oppgave: Summer totalt antall bokstaver i alle navnene til sammen
        //Bonus: Skriv ut navnene til alle som får bokstavene i navnet sitt telt opp til System.out

        /** ## Din kode her ## **/

        int sum = 0; //<--- Bytt ut 0 med din kode

        /** ## Din kode her ## **/

        assertEquals("BettineRobertAnne", systemOutRule.getLog());
        assertEquals(17, sum);

    }


    @Test
    public void createMapIndexedInNameWithLengthAsValue() {
        Stream<String> stream = Stream.of("Bettine", "Robert", "Anne");

        //Oppgave: Lag et map med navn som key, og antall tegn i navnet som value og print dette til System.out.

        /** ## Din kode her ## **/



        /** ## Din kode her ## **/


        assertEquals("[Bettine=7][Robert=6][Anne=4]", systemOutRule.getLog());
    }

    @Test
    public void testCollectors() {
        List<String> list = Arrays.asList("Taco", "Pizza", "Ost", "Is", "Kake", "Pai");

        //Oppgave: Transformer "list" til å være lik den som er i assert

        /** ## Din kode her ## **/

        List<String> transformed = Collections.EMPTY_LIST; //<--- Bytt ut Collections.EMPTY_LIST med din kode

        /** ## Din kode her ## **/

        assertThat(transformed, is(Arrays.asList("Is", "Ost", "Pai")));
    }

    @Test
    public void testAdvancedCollectors() {
        List<List<String>> collection = Arrays.asList(Arrays.asList("Anne", "Espen"), Arrays.asList("Kalle", "Ole", "Bente"));

        //Oppgave: Lag en liste med alle navnene som er sortert alfabetisk på navn

        /** ## Din kode her ## **/

        List<String> transformed = Collections.EMPTY_LIST; //<--- Bytt ut Collections.EMPTY_LIST med din kode

        /** ## Din kode her ## **/

        assertThat(transformed, is(Arrays.asList("Anne", "Bente", "Espen", "Kalle", "Ole")));
    }

    @Test
    public void testCollectSkills() {
        class Feature {
            String name;

            public Feature(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
        }

        List<Feature> features = Arrays.asList(new Feature("Streams"), new Feature("Lambda"), new Feature("java.time"));


        //Oppgave: Lag en string med navnet på alle features, med bindestrek mellom

        /** ## Din kode her ## **/

        String result = ""; //<--- Bytt ut "" med din kode

        /** ## Din kode her ## **/

        assertEquals("Streams-Lambda-java.time", result);
    }

    @Test
    public void testPrimitiveStatistics() {
        //Oppgave: Generer en liste som inneholder alle integers mellom 1 -> 50 000

        /** ## Din kode her ## **/

        List<Integer> listOf50000Integers = Collections.EMPTY_LIST; //<--- Bytt ut Collections.EMPTY_LIST med din kode

        /** ## Din kode her ## **/


        assertEquals("List size must be 50000", 50_000, listOf50000Integers.size());
        for (int i = 1; i <= 50000; i++) {
            assertEquals("Could not find integer: " + i, true, listOf50000Integers.contains(i));
        }

    }

    @Test
    public void testInfinity() {
        IntStream stream = IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        //Oppgave: Finn statistikk om tallene i streamen

        /** ## Din kode her ## **/

        IntSummaryStatistics result = new IntSummaryStatistics(); //<--- Bytt ut  new IntSummaryStatistics() med din kode

        /** ## Din kode her ## **/

        assertEquals(5.5d, result.getAverage(), 0.1d);
        assertEquals(10, result.getCount());
        assertEquals(10, result.getMax());
        assertEquals(1, result.getMin());
        assertEquals(55, result.getSum());
    }


    @Test
    public void dateFormatLong() {
        LocalDateTime dateTime = LocalDateTime.of(2016, Month.MARCH, 1, 15, 20, 30);

        //Oppgave: Lag en date time formattere som oppfyller kravet til asserten.

        /** ## Din kode her ## **/

        DateTimeFormatter df = DateTimeFormatter.ISO_DATE; //<--- Bytt ut new DateTimeFormatter.ISO_DATE med din kode

        /** ## Din kode her ## **/

        assertEquals("1. mars 2016 15:20:30", df.format(dateTime));
    }

    @Test
    public void dateFormatShort() {
        LocalDateTime dateTime = LocalDateTime.of(2016, Month.MARCH, 1, 15, 20, 30);

        //Oppgave: Lag en date time formattere som oppfyller kravet til asserten.

        /** ## Din kode her ## **/

        DateTimeFormatter df = DateTimeFormatter.ISO_DATE; //<--- Bytt ut new DateTimeFormatter.ISO_DATE med din kode

        /** ## Din kode her ## **/

        assertEquals("01.03.16 15:20", df.format(dateTime));
    }

    @Test
    public void dateFormatCustom() {
        LocalDateTime dateTime = LocalDateTime.of(2016, Month.MARCH, 1, 15, 20, 30);

        //Oppgave: Lag en date time formattere som oppfyller kravet til asserten.

        /** ## Din kode her ## **/

        DateTimeFormatter df = DateTimeFormatter.ISO_DATE; //<--- Bytt ut new DateTimeFormatter.ISO_DATE med din kode

        /** ## Din kode her ## **/

        assertEquals("01.03.2016 - 15:20:30", df.format(dateTime));

    }

}